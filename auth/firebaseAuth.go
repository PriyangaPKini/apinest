package auth

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

type FirebaseAuthParams struct {
	ProjectID string
}

// CertsAPIEndpoint is endpoint of getting Public Key.
var CertsAPIEndpoint = "https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com"

func getCertificates() (certs map[string]string, err error) {
	res, err := http.Get(CertsAPIEndpoint)

	if err != nil {
		return nil, err
	}

	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	json.Unmarshal(data, &certs)
	return
}

func getCertificate(kid string) (cert []byte, err error) {
	certs, err := getCertificates()
	if err != nil {
		return
	}
	certString := certs[kid]
	return []byte(certString), nil
}

// GetCertificateFromToken returns cert from token.
func GetCertificateFromToken(token *jwt.Token) ([]byte, error) {
	// Get kid
	kid, ok := token.Header["kid"]
	if !ok {
		return []byte{}, fmt.Errorf("kid not found")
	}
	kidString, ok := kid.(string)
	if !ok {
		return []byte{}, fmt.Errorf("kid cast error to string")
	}
	return getCertificate(kidString)
}

// Verify the token payload.
func (a *FirebaseAuthParams) verifyPayload(t *jwt.Token) (ok bool, uid string) {
	claims, ok := t.Claims.(jwt.MapClaims)
	if !ok {
		return
	}
	// Verify User
	claimsAud, ok := claims["aud"].(string)
	if claimsAud != a.ProjectID || !ok {
		return
	}
	// Verify issued at
	iss := "https://securetoken.google.com/" + a.ProjectID
	claimsIss, ok := claims["iss"].(string)
	if claimsIss != iss || !ok {
		return
	}
	// sub is uid of user.
	uid, ok = claims["sub"].(string)
	if !ok {
		return
	}
	return
}

func readPublicKey(cert []byte) (*rsa.PublicKey, error) {
	publicKeyBlock, _ := pem.Decode(cert)
	if publicKeyBlock == nil {
		return nil, fmt.Errorf("invalid public key data")
	}
	if publicKeyBlock.Type != "CERTIFICATE" {
		return nil, fmt.Errorf("invalid public key type: %s", publicKeyBlock.Type)
	}
	c, err := x509.ParseCertificate(publicKeyBlock.Bytes)
	if err != nil {
		return nil, err
	}
	publicKey, ok := c.PublicKey.(*rsa.PublicKey)
	if !ok {
		return nil, fmt.Errorf("not RSA public key")
	}
	return publicKey, nil
}

func (a *FirebaseAuthParams) verifyJWT(tokenString string) (uid string, ok bool) {
	parsed, _ := jwt.Parse(tokenString, func(t *jwt.Token) (interface{}, error) {
		ok := t.Valid
		if !ok {
			return nil, fmt.Errorf("error parsing the token")
		}
		cert, err := GetCertificateFromToken(t)
		if err != nil {
			return "", fmt.Errorf("error getting certification: %s", err)
		}
		publicKey, err := readPublicKey(cert)
		if err != nil {
			return "", fmt.Errorf("error loading public key: %s", err)
		}
		return publicKey, nil
	})

	if parsed == nil {
		ok = false
		return
	}

	// Verify header.
	if parsed.Header["alg"] != "RS256" {
		ok = false
		return
	}
	// Verify payload.
	ok, uid = a.verifyPayload(parsed)
	return
}

// Firebase Auth
func FirebaseAuth(authParams FirebaseAuthParams, strict ...bool) func(next http.Handler) http.Handler {
	// Configured middleware
	return func(next http.Handler) http.Handler {
		fn := func(res http.ResponseWriter, req *http.Request) {
			authHeader := req.Header.Get("Authorization")

			// Test if authorization header present
			if len(authHeader) > 0 {
				tokenArray := strings.Split(authHeader, " ")

				// Test if the header is in the form Bearer<space><token>
				if len(tokenArray) < 2 || tokenArray[0] != "Bearer" {
					res.WriteHeader(401)
					message := []byte("Invalid Authorization Header")
					res.Write(message)
					return
				}
				_, ok := authParams.verifyJWT(tokenArray[1])

				// If the token has been revoked or verification failed
				if !ok {
					res.WriteHeader(401)
					message := []byte("Invalid IdToken")
					res.Write(message)
					return
				}

			} else if len(strict) > 0 && strict[0] { // make sure the authorization header is present in strict mode
				res.WriteHeader(401)
				message := []byte("Unauthenticated")
				res.Write(message)
				return
			}
			// If none of the above cases apply, the authorization is done and call the next handler
			next.ServeHTTP(res, req)
		}
		return http.HandlerFunc(fn)
	}
}

package nestRouter

import (
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
)

// Success condition with trimUrl=true
func TestRouter_SuccessWithTrim(t *testing.T) {
	router := NewRouter()
	pattern := "/memes"
	router.HandleFunc(pattern, MockMemeHandler(t, ""))

	// create a mock request to use
	req := httptest.NewRequest("GET", "http://testing.com"+pattern, nil)
	// create mock response writer
	res := httptest.NewRecorder()

	router.Exec().ServeHTTP(res, req)

}

// Success condition with trimUrl=false
func TestRouter_SuccessWithoutTrim(t *testing.T) {
	router := NewRouter(false)
	pattern := "/memes"
	router.HandleFunc(pattern, MockMemeHandler(t, pattern))

	// create a mock request to use
	req := httptest.NewRequest("GET", "http://testing.com"+pattern, nil)
	// create mock response writer
	res := httptest.NewRecorder()

	router.Exec().ServeHTTP(res, req)

}

// Failure condition with trimUrl=true
func TestRouter_Return404IfNoMatch(t *testing.T) {
	router := NewRouter(false)
	pattern := "/memes"
	router.HandleFunc(pattern, MockMemeHandler(t, pattern))

	// create a mock request to use
	req := httptest.NewRequest("GET", "http://testing.com"+"/something", nil)
	// create mock response writer
	res := httptest.NewRecorder()

	router.Exec().ServeHTTP(res, req)

	result := res.Result()
	// validate response status code
	if result.StatusCode != 404 {
		t.Error("Expected status code: 404, got: " + strconv.Itoa(result.StatusCode))
	}
}

func TestRouter_Return404IfNoRoutesConfigured(t *testing.T) {
	router := NewRouter(false)

	// create a mock request to use
	req := httptest.NewRequest("GET", "http://testing.com"+"/something", nil)
	// create mock response writer
	res := httptest.NewRecorder()

	router.Exec().ServeHTTP(res, req)

	result := res.Result()
	// validate response status code
	if result.StatusCode != 404 {
		t.Error("Expected status code: 404, got: " + strconv.Itoa(result.StatusCode))
	}
}

func MockMemeHandler(t *testing.T, expectedRoute string) func(w http.ResponseWriter, r *http.Request) {

	return func(res http.ResponseWriter, req *http.Request) {
		if req.URL.Path != expectedRoute {
			t.Error("Expected Route: \"" + expectedRoute + "\", got: \"" + req.URL.Path + "\"")
		}
	}
}
